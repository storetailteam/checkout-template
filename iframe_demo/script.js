window.addEventListener("resize", function() {
    Array.prototype.slice.call(document.querySelectorAll("div")).forEach(function(el) {
        var width = window.getComputedStyle(el).width;
        el.setAttribute("data-width", width);
    });
    var container =document.querySelector(".sto-__PLACEHOLDER__-container"), size = parseInt(window.getComputedStyle(container).width, 10);
    if (size < 768) {
        container.setAttribute("data-size", "1");
    } else if (size < 960 ) {

        container.setAttribute("data-size", "2");
    } else if (size < 1152 ){

        container.setAttribute("data-size", "3");
    } else {
        container.setAttribute("data-size", "4");
    }
    document.querySelector("span").innerText = size;


});
document.querySelector(".count-selector").addEventListener("click", function (e) {
    document.querySelector(".sto-__PLACEHOLDER__-container").setAttribute("data-count", e.target.getAttribute("data-count"));
});
