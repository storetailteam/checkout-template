/*jslint browser:true, nomen:true*/
"use strict";
const ctxt_pos = 0,
  ctxt_title = 1,
  ctxt_modal = 2,
  ctxt_prodlist = 3;

var $ = window.jQuery,
  sto = window.__sto,
  settings = require("./../../settings.json"),
  style = require("./main.css"),
  html = require("./main.html"),
  format = settings.format,
  placeholder = settings.name + '_' + settings.format,
  pos = settings.btfpos,
  position = settings.position_top,
  custom = settings.custom,
  redirect = settings.redirect,
  buttons = $('<div class="sto-' + placeholder + '-buttons"></div>'),
  products = [],
  target,
  version;


//myurl = myurl.split("/")[3];

try {
  target = settings.target == "_self" ? "_self" : "_blank";

  var fontRoboto = $('<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">');
  $('head').first().append(fontRoboto);

  var sto_global_modal = false;
  var sto_global_nbproducts = 0;
  var sto_global_products = [];
  var id_data = [];
  var pos;

  custom.sort(function (a, b) {
    return a[ctxt_pos] > b[ctxt_pos]
  });

  module.exports = {
    init: _init_()
  }

  function delete_cookie(name, path, domain) {
    if (GetCookie(name)) {
      document.cookie = name + "=" +
        ((path) ? ";path=" + path : "") +
        ((domain) ? ";domain=" + domain : "") +
        ";expires=Thu, 01 Jan 1970 00:00:01 GMT";
    }
  }

  function _init_() {
    sto.load(format, function (tracker) {

      var removers = {};
      removers[format] = function () {
        style.unuse();
        container.remove();
      };


      var container = $(html),
        helper_methods = sto.utils.retailerMethod,
        url_crawling;


      helper_methods.crawl(settings.crawl).promise.then(function (d) {

        try {
          var modalCounter = 0,
            currentModal,
            currentProdsList,
            z,
            y,
            count = custom.length;

          for (z = 0; z < count; z++) {
            modalCounter = 0,
              currentModal = custom[z][ctxt_modal];
            currentProdsList = custom[z][ctxt_prodlist];
            sto_global_products = sto_global_products.concat(currentProdsList);
            if (currentModal == true || currentModal == "true") {
              for (y = 0; y < custom[z][ctxt_prodlist].length; y++) {
                $.each(d, function (ind, val) {
                  if (ind == custom[z][ctxt_prodlist][y]) {
                    modalCounter++;
                  }
                });
              }
              if (modalCounter < 1) {
                tracker.error({
                  "tl": "mandatoryNotFound"
                });
                return removers;
              }
            }
          }

          var cf = helper_methods.createFormat(container, tracker, sto_global_products, d, 100);

          style.use();

          container.attr('format', format);

          $('.checkoutPreviousHeader').after(container);

          // switch (format) {
          //   case "SE":
          //     $(container).addClass('productGrid');
          //     $('.paginationBar.top').eq(1).parent().before(container);
          //     break;
          //
          //   case "IM":
          //     if ($('.mobile-QuickAccessButtons').length > 0) {
          //
          //     } else {
          //       $('body').eq(0).append(container);
          //     }
          //     break;
          //
          //   case "SR":
          //
          //     if (pos === 'top') {
          //       $(container).addClass('productGrid');
          //       $('.paginationBar.top').eq(1).parent().before(container);
          //
          //     } else if (pos === 'mid') {
          //       if (window.matchMedia("(max-width: 719px)").matches) {
          //         $('.fila4').eq(0).find('.span-5').eq(2).before(container);
          //       } else {
          //         $('.fila4').eq(1).before(container);
          //       }
          //       window.addEventListener("resize", function() {
          //         if (window.matchMedia("(max-width: 719px)").matches) {
          //           $('.fila4').eq(0).find('.span-5').eq(2).before(container);
          //         } else {
          //           $('.fila4').eq(1).before(container);
          //         }
          //       });
          //     }
          //     break;
          //
          //   case "CH":
          //     $('.checkoutProgress').eq(0).prepend(container);
          //     break;
          // }



          // <editor-fold> PRODUCT BOX *****************************************************************************
          $('.sto-' + placeholder + '-container .sto-product-container .productGridItem').each(function () {
            var imgProduct = $(this).find('.thumb>img').attr('data-blzsrc');
            $(this).find('.thumb>img').attr('src', imgProduct);
            var imgIcon = $(this).find('.thumb .icons-vertical-right>img').attr('data-blzsrc');
            $(this).find('.thumb .icons-vertical-right>img').attr('src', imgIcon);
          })


          // </editor-fold> ****************************************************************************************



          $('.sto-' + placeholder + '-container[format="IM"] .sto-' + placeholder + '-close').click(function () {
            $(this).attr('data-state', 'sto-closed')
            $(this).closest('.sto-' + placeholder + '-container[format="IM"]').animate({
              bottom: '-120px'
            }, 700, function () {
              tracker.close();
              $('.sto-' + placeholder + '-container[format="IM"]').remove();
              if ($('#sto-gutters').length > 0) {
                $('#sto-gutters').remove();
              }
            })
          });

          for (var i = 0; i < $(container).find('.sto-product-container>li').length; i++) {
            if ($(container).find('.sto-product-container>li').eq(i).find('.blocPriceBorder').length > 0) {
              $(container).find('.sto-product-container>li').eq(i).find('.blocPriceBorder').eq(0).before('<div class="sto-price-wrapper"></div>');
              var priceDetach = $(container).find('.sto-product-container>li').eq(i).find('.blocPriceBorder').detach();
              $('.sto-price-wrapper').append(priceDetach);
            }
          }

          for (var i = 0; i <= count; i++) {
            browseObj(i, d, tracker);
          }

          $(".sto-" + placeholder + "-buttons").attr('data-btn', settings.btnSize);

          $('.sto-' + placeholder + '-container').attr('data-type', '1');

          if (settings.custom_bg === true) {
            $('.sto-' + placeholder + '-container').attr('data-ind', '0');
          }
          if (settings.custom_logo === true) {
            $('.sto-' + placeholder + '-logo').attr('data-ind', '0');
          }


          var prodCount = $('.sto-' + placeholder + '-container li.clearfix.Article-item').length;
          for (var x = 0; x < prodCount; x++) {
            $($('.sto-' + placeholder + '-container li.clearfix.Article-item')[x]).attr('data-ind', x);
          }

          for (var i = 0; i < $('.sto-product-container .thumbnail-imgContent').length; i++) {
            if ($('.sto-product-container .thumbnail-imgContent').eq(i).hasClass('js-LazyImage')) {
              var imgUrl = $('.sto-product-container .thumbnail-imgContent').eq(i).attr('data-lazyimage-background');
              $('.sto-product-container .thumbnail-imgContent').eq(i).attr('src', imgUrl);
            }
          }

          /*var pointsTitle = function () {
              if ($('.sto-product-container>div:visible>.productMainLink>.details>.productName>span').height() > 35) {
                  $('.sto-product-container>div:visible>.productMainLink>.details>.productName>span').addClass('sto-text-too-big');
              }
          };
          pointsTitle();*/

          function setMiniTitle() {
            $(".sto-" + placeholder + "-container .sto-product-container > div").each(function (i, v) {
              var title = $(v).find(" > .productMainLink>.details>.productName>span");
              var dTitle, mTitle;
              dTitle = mTitle = title[0].innerText;

              if (dTitle.length > 65) {
                //dTitle = dTitle.substring(0, 30)+"..."
                $(title).text(dTitle.substring(0, 65) + "...");
              }
              //if(mTitle.length > 60){mTitle = mTitle.substring(0, 60)+"..."}

            });
          }
          setMiniTitle();

          var indexProd;
          // Create a button for each product on the format
          var navButtons = function () {
            var c, button, nameProd, idProd;

            // get the active button
            $('.sto-' + placeholder + '-button').first().addClass('sto-' + placeholder + '-button-current');
            $('.sto-' + placeholder + '-button').on('click', function () {
              indexProd = $(this).attr('data-ind');
              idProd = $(this).attr('data-prod');

              if (settings.custom_bg === true) {
                $('.sto-' + placeholder + '-container').attr('data-ind', indexProd);
              }
              if (settings.custom_logo === true) {
                $('.sto-' + placeholder + '-logo').attr('data-ind', indexProd);
              }

              $('.sto-' + placeholder + '-button').removeClass('sto-' + placeholder + '-button-current');
              $(this).addClass('sto-' + placeholder + '-button-current');

              cf.changeState(idProd);

              //pointsTitle();

              tracker.browse({
                "te": idProd
              });
            });
          };


          // <editor-fold> CTA *************************************************************************************
          // Call to action
          if (settings.cta_type == "redirect" || settings.cta_type == "ajout" || settings.cta_type == "legal" || settings.cta_type == "video" || settings.cta_type == "pdf") {
            $(".sto-" + placeholder + "-buttons").append("<span class='sto-" + placeholder + "-link'><span>" + settings.text_recette + "<span></span>");
            if (settings.download_img != "") {
              $('.sto-' + placeholder + '-link').attr('data-ico', 'dl');
            }
          }

          var ctaType = function () {
            var cta = settings.cta_type;

            $(".sto-" + placeholder + "-link").attr("data-type", settings.cta_type);

            switch (cta) {
              case "redirect":
                if (settings.download_img == "") {
                  //$(".sto-" + placeholder + "-link[data-type='redirect']").css('padding-left','5px')
                }
                $(".sto-" + placeholder + "-link").on("click", function () {
                  if (settings.redirect !== "") {
                    window.open(settings.redirect, settings.target);
                    tracker.click();
                  }
                });
                break;
              case "pdf":
                if (settings.download_img == "") {
                  //$(".sto-" + placeholder + "-link[data-type='redirect']").css('padding-left','5px')
                }
                $(".sto-" + placeholder + "-link").on("click", function () {
                  if (settings.file !== "") {
                    var pdf = require("../../img/" + settings.file);
                    window.open(pdf, "_blank");
                    tracker.openPDF();
                  }
                });
                break;
              case "video":
                $(".sto-" + placeholder + "-link").on("click", function () {
                  $("body").prepend('<div class="sto-ecran"></div><div class="sto-video"><div class="sto-close"></div>' + settings.video + '</div>');
                  var video_margin = parseInt($(".sto-video").width()) / 2;
                  $(".sto-video").css("left", "-" + video_margin + "px");
                  tracker.playVideo();

                  $("body")
                    .on("click", ".sto-ecran", function () {
                      $("body .sto-ecran").remove();
                      $("body .sto-video").remove();
                      tracker.closeVideo({
                        "tz": "closeVideoBody"
                      });
                    })
                    .on("click", ".sto-close", function () {
                      $("body .sto-ecran").remove();
                      $("body .sto-video").remove();
                      tracker.closeVideo({
                        "tz": "closeVideoBtn"
                      });
                    });
                });
                break;

              case "ajout":

                $(".sto-" + placeholder + "-link").on("click", function () {
                  var prodsID = [];
                  for (var i = 0; i < $('.sto-product-container .thumbnail-btn.thumbnail-btnCart').length; i++) {
                    var prodTracker = {
                      "id": $('.sto-product-container .thumbnail-btn.thumbnail-btnCart').eq(i).attr('data-prid'),
                      "qty": 1,
                      "promo": d[$('.sto-product-container .thumbnail-btn.thumbnail-btnCart').eq(i).attr('data-prid')]['promo'],
                      "name": $('.sto-product-container .thumbnail-btn.thumbnail-btnCart').eq(i).closest('.thumbnail').find('.thumbnail-titleLink').text(),
                      "price": parseFloat($('.sto-product-container .thumbnail-btn.thumbnail-btnCart').eq(i).closest('.thumbnail').find('.thumbnail-price').text())
                    };
                    prodsID.push(prodTracker);
                  }
                  tracker.addAllToBasket(prodsID);
                  $('.sto-product-container .thumbnail-btn.thumbnail-btnCart').click();
                });
                break;
              case "legal":
                break;
              default:
                break;
            }

          };


          $("body").on("click", ".sto-" + placeholder + "-container[version='mobile'] .sto-" + placeholder + "-logo", function () {
            // alert("in bind click");
            $(".sto-" + placeholder + "-link").click();
          });

          $("body").on("click", ".sto-" + placeholder + "-container[data-size='1'] .sto-" + placeholder + "-logo", function () {
            // alert("in bind click");
            $(".sto-" + placeholder + "-link").click();
          });
          // </editor-fold> ****************************************************************************************



          // <editor-fold> RESIZE **********************************************************************************
          // Deal with window size
          var viewport, data_size;
          window.addEventListener("resize", function () {

            // Deal with format size
            var size = $("#sto-format.sto-"+placeholder+"-container").innerWidth();

            if(size < 576){
              data_size = 1;
              version = "mobile";
            }else{
              version = "desktop";
              if (size < 768) {
                  data_size = 1;
              } else if (size < 864) {
                  data_size = 2;
              } else if (size < 960) {
                  data_size = 3;
              } else if (size < 1152) {
                  data_size = 4;
              } else if (size > 1152) {
                  data_size = 5;
              }
            }

            container.attr("data-size", data_size);

            if (version == "mobile") {
              var widthContainer = $(".sto-" + placeholder + "-container").innerWidth();
              $(container).attr("version", "mobile");
              $('.sto-' + placeholder + '-logo').css('height', (Math.floor((300 * widthContainer) / 1242)) + 'px');
              $('.sto-' + placeholder + '-buttons').css('height', (Math.floor((300 * widthContainer) / 1242)) + 'px');
              $('.sto-' + placeholder + '-buttons').css('top', "-" + (Math.floor((300 * widthContainer) / 1242)) + 'px');
              $('.sto-' + placeholder + '-carrousel').css('margin-top', (Math.floor((300 * widthContainer) / 1242)) + 'px');


              var marginCarrousel = $('.sto-' + placeholder + '-carrousel').css('margin-top')
              //$('.sto-' + placeholder + '-container[data-size="0"] .sto-' + placeholder + '-format').css("background-size", '100% ' + marginCarrousel);
              //$('.sto-' + placeholder + '-logo').css("background-size", '100% ' + marginCarrousel);

            } else {
              $(container).attr("version", "desktop");
              $('.sto-' + placeholder + '-logo').css('height', '120px');
              $('.sto-' + placeholder + '-buttons').css('height', 'inherit');
              $('.sto-' + placeholder + '-buttons').css('top', 'inherit');
              $('.sto-' + placeholder + '-carrousel').css('margin-top', '0');
              $('.sto-' + placeholder + '-logo').css("background-size", 'contain');
              $('.sto-' + placeholder + '-format').css("background-size", 'cover');
            }

            // var windowWidth = $(window).width();
            //
            // if (format == "SR") {
            //   if (windowWidth < 720 && $('.sto-__PLACEHOLDER__-container').prevAll()) {
            //       $('.productGridItem').eq(2).parent().before(container);
            //   } else {
            //       $('.productGridItem').eq(4).parent().before(container);
            //   }
            // }

          });
          // </editor-fold> ****************************************************************************************


          window.setTimeout(function () {
            $('.sto-' + placeholder + '-container[format="IM"]').animate({
              bottom: 0
            }, 700)
            var evt = window.document.createEvent('UIEvents');
            evt.initUIEvent('resize', true, false, window, 0);
            window.dispatchEvent(evt);

          });

          var launch = setInterval(function () {
            if ($('.sto-product-container')) {
              navButtons();
              ctaType();
              if ($(".sto-" + placeholder + "-link").length == 0) {
                var finalCount = $('.sto-' + placeholder + '-container .sto-' + placeholder + '-button').length;
                $('.sto-' + placeholder + '-container').attr('data-link', 'false');
              } else {
                var finalCount = $('.sto-' + placeholder + '-container .sto-' + placeholder + '-button').length + 1;
                $('.sto-' + placeholder + '-container').attr('data-link', 'true');
              }
              $('.sto-' + placeholder + '-container').attr('data-count', finalCount);
              clearInterval(launch);
            } else {
              setTimeout(function () {
                clearInterval(launch);
              }, 2000);
            }
          }, 100);
        } catch (e) {
          tracker.error({
            "te": "onBuild-format",
            "tl": e
          });
        }
      }).then(null, function () {
        tracker.error({
          "te": "noproduct"
        })
      });

      return removers;
    });
  }

  function verifAvailability(a, d) {
    var l = a[ctxt_prodlist].length;
    for (var i = 0; i <= (l - 1); i++) {
      var value = a[ctxt_prodlist][i];
      if (d[value] !== undefined) {
        sto_global_products.push(value);
        sto_global_nbproducts += 1;
        return value;
      }
    }
    return false;
  }

  function browseObj(index, d, tracker) {
    var currentbObj = custom[index];
    if (currentbObj) {
      var title = currentbObj[ctxt_title],
        modal = currentbObj[ctxt_modal],
        available = verifAvailability(currentbObj, d);
      modal = modal === "true" || modal === true ? true : false;
      if (available != false) {
        if (modal) {
          sto_global_modal = true;
        }
        addButton(available, title, index, tracker);
      }
    }

  }
  var prodMax = 0;

  function addButton(cug, title, index) {
    if (settings.cta_type == "") {
      $('.sto-' + placeholder + '-buttons').append('<button class="sto-' + placeholder + '-button" data-prod="' + cug + '"><span>' + title + '<span></button>');
      prodMax++;
      $('.sto-' + placeholder + '-buttons').attr("size-product", prodMax);
    } else if (prodMax < 5) {
      $('.sto-' + placeholder + '-buttons').append('<button class="sto-' + placeholder + '-button" data-prod="' + cug + '"><span>' + title + '<span></button>');
      prodMax++;
      $('.sto-' + placeholder + '-buttons').attr("size-product", prodMax);
    }
  }

} catch (e) {
  tracker.error({
    "te": "onBuild-environnement",
    "tl": e
  });
}
