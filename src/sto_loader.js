var settings = require("./settings.json");
settings.border_color = settings.border_color === "" ? "#d2d2d2" : settings.border_color;
settings.bg_container_color_left = settings.bg_container_color_left === "" ? "#ffffff" : settings.bg_container_color_left;
settings.bg_container_color_right = settings.bg_container_color_right === "" ? "#ffffff" : settings.bg_container_color_right;
settings.redirect_txt_color = settings.redirect_txt_color === "" ? "transparent" : settings.redirect_txt_color;
settings.btf_container_img = settings.btf_container_img === "" ? "none" : "url(./../../img/" + settings.btf_container_img + ")";

settings.btf_container_img_mobile = settings.btf_container_img_mobile === "" ? "none" : "url(./../../img/" + settings.btf_container_img_mobile + ")";

settings.arrow_left_img = settings.arrow_left_img === "" ? "none" : "url(./../../img/" + settings.arrow_left_img + ")";
settings.arrow_left_hover_img = settings.arrow_left_hover_img === "" ? "none" : "url(./../../img/" + settings.arrow_left_hover_img + ")";
settings.carrousel_img = settings.carrousel_img === "" ? "none" : "url(./../../img/" + settings.carrousel_img + ")";
settings.main_product_img = settings.main_product_img === "" ? "none" : "url(./../../img/" + settings.main_product_img + ")";
settings.main_product_small_img = settings.main_product_small_img === "" ? "none" : "url(./../../img/" + settings.main_product_small_img + ")";
settings.logo_img = settings.logo_img === "" ? "none" : "url(./../../img/" + settings.logo_img + ")";
settings.logo_img_mob = settings.logo_img_mob === "" ? "none" : "url(./../../img/" + settings.logo_img_mob + ")";
settings.design_left_img = settings.design_left_img === "" ? "none" : "url(./../../img/"+settings.design_left_img+")";
settings.design_left_img2 = settings.design_left_img2 === "" ? "none" : "url(./../../img/"+settings.design_left_img2+")";
settings.design_right_img = settings.design_right_img === "" ? "none" : "url(./../../img/"+settings.design_right_img+")";
settings.buttons_width = settings.buttons_width === "" ? "125px" : settings.buttons_width;
settings.button_ico = settings.button_ico === "" ? "none" : "url(./../../img/" + settings.button_ico + ")";
settings.buttons_couleur = settings.buttons_couleur === "" ? "transparent" : settings.buttons_couleur;
settings.buttons_couleur_select = settings.buttons_couleur_select === "" ? "transparent" : settings.buttons_couleur_select;
settings.buttons_img = settings.buttons_img === "" ? "none" : "url(./../../img/" + settings.buttons_img + ")";
settings.buttons_img_select = settings.buttons_img_select === "" ? "none" : "url(./../../img/" + settings.buttons_img_select + ")";
settings.buttons_txt = settings.buttons_txt === "" ? "none" : settings.buttons_txt;
settings.buttons_txt_select = settings.buttons_txt_select === "" ? "none" : settings.buttons_txt_select;
settings.download_img = settings.download_img === "" ? "none" : "url(./../../img/"+settings.download_img+")";
settings.format_img_1 = settings.format_img_1 === "" ? " " : "url(./../../img/" + settings.format_img_1 + ")";
settings.format_img_2 = settings.format_img_2 === "" ? " " : "url(./../../img/" + settings.format_img_2 + ")";
settings.format_img_3 = settings.format_img_3 === "" ? " " : "url(./../../img/" + settings.format_img_3 + ")";
settings.format_img_4 = settings.format_img_4 === "" ? " " : "url(./../../img/" + settings.format_img_4 + ")";
settings.format_img_5 = settings.format_img_5 === "" ? " " : "url(./../../img/" + settings.format_img_5 + ")";
settings.close_img = settings.close_img === "" ? " " : "url(./../../img/" + settings.close_img + ")";
settings.close_img_hover = settings.close_img_hover === "" ? " " : "url(./../../img/" + settings.close_img_hover + ")";
settings.buttons_police_size = settings.buttons_police_size === "" ? "11.5" : settings.buttons_police_size;
module.exports = function(source) {
    this.cacheable();
    var test = source
        .replace(/__PLACEHOLDER__/g, settings.name+'_'+settings.format)
        .replace(/__BORDER_CONTAINER_COLOR__/g, settings.border_color)
        .replace(/__BG_CONTAINER_COLOR_LEFT__/g, settings.bg_container_color_left)
        .replace(/__BG_CONTAINER_COLOR_RIGHT__/g, settings.bg_container_color_right)
        .replace(/__REDIRECT_TXT_COLOR__/g, settings.redirect_txt_color)
        .replace(/__BG_FORMAT_DSK_IMG__/g, settings.btf_container_img)
        .replace(/__BG_FORMAT_MOB_IMG__/g, settings.btf_container_img_mobile)
        .replace(/__ARROW_LEFT_IMG__/g, settings.arrow_left_img)
        .replace(/__ARROW_LEFT_HOVER_IMG__/g, settings.arrow_left_hover_img)
        .replace(/__CARROUSEL_IMG__/g, settings.carrousel_img)
        .replace(/__MAINPROD_IMG__/g, settings.main_product_img)
        .replace(/__MAINPROD_SMALL_IMG__/g, settings.main_product_small_img)
        .replace(/__LOGO_IMG__/g, settings.logo_img)
        .replace(/__LOGO_IMG_MOB__/g, settings.logo_img_mob)
        .replace(/__DESIGN_LEFT_IMG__/g, settings.design_left_img)
        .replace(/__DESIGN_LEFT_IMG2__/g, settings.design_left_img2)
        .replace(/__DESIGN_RIGHT_IMG__/g, settings.design_right_img)
        .replace(/__DL_IMG__/g,settings.download_img)
        .replace(/__BUTTON_WIDTH__/g, settings.buttons_width)
        .replace(/__BUTTON_ICO_IMG__/g, settings.button_ico)
        .replace(/__BUTTON_BG_NORMAL_COLOR__/g, settings.buttons_couleur)
        .replace(/__BUTTON_BG_HOVER_COLOR__/g, settings.buttons_couleur_select)
        .replace(/__BUTTON_BG_NORMAL_IMG__/g, settings.buttons_img)
        .replace(/__BUTTON_BG_HOVER_IMG__/g, settings.buttons_img_select)
        .replace(/__BUTTON_TXT_NORMAL_COLOR__/g, settings.buttons_txt)
        .replace(/__BUTTON_TXT_HOVER_COLOR__/g, settings.buttons_txt_select)
        .replace(/__BUTTON_POLICE_SIZE__/g, settings.buttons_police_size)
        .replace(/__BG_IMG_01__/g, settings.format_img_1)
        .replace(/__BG_IMG_02__/g, settings.format_img_2)
        .replace(/__BG_IMG_03__/g, settings.format_img_3)
        .replace(/__BG_IMG_04__/g, settings.format_img_4)
        .replace(/__BG_IMG_05__/g, settings.format_img_5)
        .replace(/__CLOSE_BTN_IMG__/g, settings.close_img)
        .replace(/__CLOSE_BTN_HOVER_IMG__/g, settings.close_img_hover);
    return test;
};
